const jwt = require('jsonwebtoken');
const User = require(`${config.path.model}/user`);

module.exports = (req , res , next) =>  {
    let token = req.headers['x-access-token'];

    if(token) {
        return jwt.verify(token , config.secret , (err , decode ) => {
            if(err) {
                return res.json(
                    utils.responseObj("Failed to authenticate token.", {}, false)
                );
            } 
            
            User.findById(decode.user_id , (err , user) => {
                if(err) throw err;

                if(user) {
                    user.token = token;
                    req.user = user;
                    next();
                } else {
                    return res.json(
                        utils.responseObj("User not found.", {}, false)
                    );
                }
            }) 

        })
    }

    return res.status(403).json(
        utils.responseObj("No Token Provided.", {}, false)
    );

}