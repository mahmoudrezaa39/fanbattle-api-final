module.exports = (req , res , next) =>  {
    if(req.user.type == config.roles_name.tester) {
        next();
        return;
    }

    return res.json(
        utils.responseObj("access denied.", {}, false)
    );
}