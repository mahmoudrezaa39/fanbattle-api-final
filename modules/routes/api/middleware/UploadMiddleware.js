const multer  = require('multer')

const ImageStorage = multer.diskStorage({
    filename: (req , file , cb) => {
        cb(null, (file.fieldname + '-' + Date.now() +  '-' + file.originalname.replace(/[ !$%^&*()_+|~=`{}\[\]:";'<>?,\/]/g, '-')))
    }
});

const imageFilter = (req , file , cb) => {
    if(file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "video/mp4") {
        cb(null , true)
    } else {
        req.fileValidationError = "Forbidden extension! Correct formats: png & jpeg";
        return cb(null , false, req.fileValidationError)
    }
}

const uploadImage = multer({ 
    storage : ImageStorage,
    fileFilter : imageFilter
})


module.exports = {
    uploadImage
}