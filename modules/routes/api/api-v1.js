const express = require('express');
const router = express.Router();
const adminRouter = express.Router();
const developerRouter = express.Router();
const testerRouter = express.Router();
const { api : ControllerApi } = config.path.controllers;

// middlewares 
const apiAuth = require('./middleware/apiAuth');
const apiAdmin = require('./middleware/apiAdmin');
const apiDeveloper = require('./middleware/apiDeveloper');
const apiTester = require('./middleware/apiTester');
const { uploadImage } = require('./middleware/UploadMiddleware');

// Controllers 
const HomeController = require(`${ControllerApi}/v1/HomeController`);
const AuthController = require(`${ControllerApi}/v1/AuthController`);
const UserController = require(`${ControllerApi}/v1/UserController`);

router.get('/' , HomeController.index.bind(HomeController));
router.post('/signin' , AuthController.login.bind(AuthController));
router.post('/social-signin' , AuthController.socialLogin.bind(AuthController));
router.post('/signup' , AuthController.register.bind(AuthController));
router.get('/get-user' , apiAuth , UserController.getUserInfo.bind(UserController));
router.get('/get-one-test' , apiAuth , UserController.getOneTest.bind(UserController));
router.get('/get-all-gift-model' , apiAuth , UserController.getAllGiftModel.bind(UserController));
router.get('/get-file/:file_name' , apiAuth , UserController.retrieveFile.bind(UserController));
router.get('/get-one-gift-card' , apiAuth , UserController.getOneGiftCard.bind(UserController));
router.get('/get-one-feedback/:feedback_id' , apiAuth , UserController.getOneFeedback.bind(UserController));

// AdminController
const AdminHomeController = require(`${ControllerApi}/v1/admin/AdminHomeController`);
const AdminGiftCardController = require(`${ControllerApi}/v1/admin/AdminGiftCardController`);
const AdminTestController = require(`${ControllerApi}/v1/admin/AdminTestController`);

adminRouter.get('/' , AdminHomeController.index.bind(AdminHomeController));
adminRouter.post('/create-gift-model' , AdminGiftCardController.createGiftModel.bind(AdminGiftCardController));
adminRouter.put('/edit-gift-model/:gift_model_id' , AdminGiftCardController.editGiftModel.bind(AdminGiftCardController));
adminRouter.delete('/delete-gift-model/:gift_model_id' , AdminGiftCardController.deleteGiftModel.bind(AdminGiftCardController));
adminRouter.post('/create-gift-card' , AdminGiftCardController.createGift.bind(AdminGiftCardController));
adminRouter.post('/create-list-of-gift-card' , AdminGiftCardController.createListOfGift.bind(AdminGiftCardController));
adminRouter.put('/edit-gift-card/:gift_id' , AdminGiftCardController.editGift.bind(AdminGiftCardController));
adminRouter.delete('/delete-gift-card/:gift_id' , AdminGiftCardController.deleteGift.bind(AdminGiftCardController));
adminRouter.get('/get-all-gift-card' , AdminGiftCardController.getListOfGift.bind(AdminGiftCardController));
adminRouter.put('/public-test/:test_id' , AdminTestController.publicTest.bind(AdminTestController));
adminRouter.get('/get-all-test' , AdminTestController.getAllTest.bind(AdminTestController));
adminRouter.get('/get-all-transaction' , AdminHomeController.getAllTransaction.bind(AdminHomeController));
adminRouter.get('/get-all-feedback' , AdminTestController.getAllFeedback.bind(AdminTestController));

// DeveloperController
const DeveloperHomeController = require(`${ControllerApi}/v1/developer/DeveloperHomeController`);
const DeveloperTestController = require(`${ControllerApi}/v1/developer/DeveloperTestController`);
const DeveloperPaymentController = require(`${ControllerApi}/v1/developer/DeveloperPaymentController`);

developerRouter.get('/' , DeveloperHomeController.index.bind(DeveloperHomeController));
developerRouter.post('/create-test' , DeveloperTestController.createTest.bind(DeveloperTestController));
developerRouter.post('/post-file/:test_id' , uploadImage.single('game_poster') , DeveloperTestController.uploadFile.bind(DeveloperTestController));
developerRouter.put('/edit-test' , DeveloperTestController.editTest.bind(DeveloperTestController));
developerRouter.get('/get-my-tests' , DeveloperTestController.getMyTests.bind(DeveloperTestController));
developerRouter.get('/get-payable-price' , DeveloperPaymentController.getPayable.bind(DeveloperPaymentController));
developerRouter.get('/buy-with-paypal/:p' , DeveloperPaymentController.buyWithPaypal.bind(DeveloperPaymentController));
developerRouter.get('/buy-success' , DeveloperPaymentController.buySuccess.bind(DeveloperPaymentController));
developerRouter.get('/buy-failed' , DeveloperPaymentController.buyFailed.bind(DeveloperPaymentController));
developerRouter.get('/get-my-transaction' , DeveloperPaymentController.getMyTransaction.bind(DeveloperPaymentController));
developerRouter.put('/confirm-feedback/:feed_id' , DeveloperTestController.confirmFeedback.bind(DeveloperTestController));

// TesterController
const TesterHomeController = require(`${ControllerApi}/v1/tester/TesterHomeController`);
const TesterProfileController = require(`${ControllerApi}/v1/tester/TesterProfileController`);
const TesterFeedbackController = require(`${ControllerApi}/v1/tester/TesterFeedbackController`);

testerRouter.get('/' , TesterHomeController.index.bind(TesterHomeController));
testerRouter.post('/create-profile' , TesterProfileController.createProfile.bind(TesterProfileController));
testerRouter.get('/get-profile' , TesterProfileController.getProfile.bind(TesterProfileController));
testerRouter.put('/edit-profile' , TesterProfileController.editProfile.bind(TesterProfileController));
testerRouter.get('/get-tests' , TesterFeedbackController.getTests.bind(TesterFeedbackController));
testerRouter.post('/create-feedback/:test_id' , TesterFeedbackController.createFeedback.bind(TesterFeedbackController));
testerRouter.get('/get-my-gifts' , TesterFeedbackController.getMyGifts.bind(TesterFeedbackController));
testerRouter.get('/get-my-feedback' , TesterFeedbackController.getMyFeedback.bind(TesterFeedbackController));
testerRouter.post('/post-file/:feedback_id' , uploadImage.single('feedback_clip') , TesterFeedbackController.uploadFile.bind(DeveloperTestController));

router.use('/admin' , apiAuth , apiAdmin , adminRouter);
router.use('/developer' , apiAuth , apiDeveloper , developerRouter);
router.use('/tester' , apiAuth , apiTester , testerRouter);

module.exports = router;