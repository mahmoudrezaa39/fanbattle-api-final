const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate-v2');

const GiftCardModelSchema = new Schema({
    creator : {type : Schema.Types.ObjectId, ref : 'User'},
    amount : {type : Number, required : true},
    type : {type : String, default : "Amazon Gift Card"}
});
GiftCardModelSchema.plugin(timestamps);
GiftCardModelSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('GiftCardModel' , GiftCardModelSchema);