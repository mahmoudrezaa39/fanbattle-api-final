const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate-v2');

const FeedbackSchema = new Schema({
    test_id : { type : Schema.Types.ObjectId , ref : 'Test' },
    tester_id : { type : Schema.Types.ObjectId , ref : 'User' },
    video_link : { type : String , required : true },
    body : { type : String , required : true },
    status : { type : Boolean , default : false }
});
FeedbackSchema.plugin(timestamps);
FeedbackSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Feedback' , FeedbackSchema);