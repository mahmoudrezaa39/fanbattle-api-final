const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');

const TesterProfileSchema = new Schema({
    tester_id : { type : Schema.Types.ObjectId , ref : 'User' },
    birthday : {
        year : { type : Number , required : true },
        month : { type : Number , required : true },
        day : { type : Number , required : true }
    } ,
    gender : { type : Number , required : true , default : -1} , // -1: men, 1: women, 0: other 
    location : { type : String , required : true}
});
TesterProfileSchema.plugin(timestamps);

module.exports = mongoose.model('TesterProfile' , TesterProfileSchema);