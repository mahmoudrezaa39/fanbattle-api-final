const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate-v2');

const GiftCardSchema = new Schema({
    creator : {type : Schema.Types.ObjectId, ref : 'User'},
    model : {type : Schema.Types.ObjectId, ref : 'GiftCardModel'},
    code : {type : String, required : true, unique : true},
    tester_id : {type : Schema.Types.ObjectId, ref : 'User', default : null},
    status : {type : String, default : 'private'} // private , public
});
GiftCardSchema.plugin(timestamps);
GiftCardSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('GiftCard' , GiftCardSchema);