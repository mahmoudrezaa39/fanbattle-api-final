const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate-v2');

const transactionSchema = new Schema({
    test_id : { type : Schema.Types.ObjectId , ref : 'Test'},
    developer_id : { type : Schema.Types.ObjectId , ref : 'User'},
    price : { type : Number , required : true },
    paymentId : { type : String },
    PayerID : { type : String , default : null },
    status : { type : Boolean , default : false }
});
transactionSchema.plugin(timestamps);
transactionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('TransactionInfo' , transactionSchema);