const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate-v2');

const TestSchema = new Schema({
    developer_id : { type : Schema.Types.ObjectId , ref : 'User'},
    tester_count : { type : Number , required : true , default : 1 },
    testers_id : [{ type : Schema.Types.ObjectId , ref : 'User'}],
    test_name : { type : String , required : true },
    game_poster : [{ type : String , required : true }],
    game_link : [{ type : String , required : true }],
    body : { type : String , required : true },
    incentive : { type : Schema.Types.ObjectId , ref : 'GiftCardModel'},
    expire_data : { type : String , default : null },
    tester_info : {
        minAge : {type : Number , required : true},
        maxAge : {type : Number , required : true},
        gender : { type : Number , default : -1 , required : true },
        location : { type : String , required : true }
    },
    feedback : [{ type : Schema.Types.ObjectId , ref : 'Feedback'}],
    status : { type : String , default : "pending" } // pending , public , expire
});
TestSchema.plugin(timestamps);
TestSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Test' , TestSchema);