const path = require('path');

module.exports = {
    port : 3000,
    domain : 'http://18.222.135.237',
    mongo_uri : 'mongodb+srv://MahmoudReza:mahmoudrezaaliz@cluster0.imqdd.mongodb.net/fanbattle?retryWrites=true&w=majority',
    secret : 'sadas@!$@#%!^#!GSDGETWT@#OI%J@#%!*#)^U#)^U!@)U',
    paypal : {
        mode : 'sandbox', //sandbox or live 
        client_id : 'AU7zzL8bL2V-bBuQB_Z1_ZYVKkBIX35Z9U7s86LVe40L84z8pbWC_cDwVYQYmiVW0_nXmNzJWbDtfkcT',
        client_secret : 'EIAHboNylAFOiY4Qq2zKiYlyUYx5yZefwrNGThlDAscwSF__FT8SXNrEXlkKFVmqRle46ZftNAYv9rP6'
    },
    s3_bucket_keys : {
        bucket_name : 'fanbattle-bucket',
        region: 'ca-central-1',
        iam_access_id:'AKIAQE2RO4TRG6VLRWX5',
        iam_secret:'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'
    },
    path : {
        controllers : { 
            api : path.resolve('./modules/controllers/api')
        },
        model : path.resolve('./modules/models'),
        transform : path.resolve('./modules/transforms'),
        controller : path.resolve('./modules/controllers'),
    },
    recruiting_fee : 1,
    item_count_in_page : 15,
    roles_name : {
        admin : 'admin',
        developer : 'developer',
        tester : 'tester'
    }
}