module.exports = class Transform {

    transformCollection(items) {
        if(this.withPaginateStatus) {
            return {
                [this.CollectionName()] : items.docs.map(this.transform.bind(this)),
                ...this.paginateItem(items)
            }
        }
        return items.map(this.transform.bind(this))
    }

    paginateItem(items) {
        return {
            totalItem : items.totalDocs,
            limit : items.limit,
            page : items.page,
            totalPages : items.totalPages,
            hasNextPage : items.hasNextPage,
            hasPrevPage : items.hasPrevPage
        }
    }

    CollectionName() {
        return 'items';
    }

    withPaginate() {
        this.withPaginateStatus = true;
        return this;
    }

}