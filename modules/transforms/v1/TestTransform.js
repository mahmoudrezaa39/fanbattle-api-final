const Transform = require('./../Transform');
module.exports = class TestTransform extends Transform {

    transform(item) {
        return {
            '_id' : item._id,
            'test_name' : item.test_name,
            'game_poster' : item.game_poster,
            'game_link' : item.game_link,
            'expire_data' : item.expire_data,
            'status' : item.status,
            'feedback' : item.feedback
        }
    }
}