const Transform = require('../Transform');
module.exports = class TransactionTransform extends Transform {

    transform(item) {
        return {
            '_id' : item._id,
            'status' : item.status,
            'price' : item.price,
            'paymentId' : item.paymentId,
            'PayerID' : item.PayerID,
            'test_id' : item.test_id
        }
    }
}