const Transform = require('./../Transform');

module.exports = class GiftCardTransform extends Transform {

    transform(item) {
        return {
            '_id' : item._id,
            'model' : item.model,
            'code' : item.code,
            'status' : item.status,
        }
    }
}