const Transform = require('../Transform');

module.exports = class GiftCardModelTransform extends Transform {

    transform(item) {
        return {
            '_id' : item._id,
            'amount' : item.amount,
            'type' : item.type
        }
    }
}