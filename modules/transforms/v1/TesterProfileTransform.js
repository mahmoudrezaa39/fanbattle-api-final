
module.exports = class TesterProfileTransform {
    transform(item) {
        return {
            'gender' : item.gender,
            'birthday' : item.birthday,
            'location' : item.location
        }
    }

}