const Transform = require('../Transform');

module.exports = class FeedbackTransform extends Transform {

    transform(item) {
        return {
            '_id' : item._id,
            'video_link' : item.video_link,
            'body' : item.body,
            'status' : item.status,
            'test' : item.test_id
        }
    }
}