const mongoose = require('mongoose');
const Controller = require(`${config.path.controller}/Controller`);
const FeedbackTransform = require(`${config.path.transform}/v1/FeedbackTransform`);
const GiftCardTransform = require(`${config.path.transform}/v1/GiftCardTransform`);
const GiftCardModelTransform = require(`${config.path.transform}/v1/GiftCardModelTransform`);
const UserTransform = require(`${config.path.transform}/v1/UserTransform`);
const TestTransform = require(`${config.path.transform}/v1/TestTransform`);
const AWS = require('aws-sdk');
const s3= new AWS.S3();

module.exports = new class UserController extends Controller {
    getUserInfo(req , res) {
        return res.json(utils.responseObj('user information', new UserTransform().transform(req.user), true))
    }

    getOneTest(req, res) {
        if(!mongoose.Types.ObjectId.isValid(req.body.test_id))
            return res.json(utils.responseObj('test_id is not valid.', {}, false))
        this.model.Test.findOne({ _id : req.body.test_id } , (err , test) => {
            if(err) throw err;
            if(test == null) {
                return res.json(
                    utils.responseObj("not found!", {}, false)
                );
            } else {
                return res.json(utils.responseObj('test information', new TestTransform().transform(test), true))
            }
        })
    }

    retrieveFile(req, res) {
        const getParams = {
            Bucket: config.s3_bucket_keys.bucket_name,
            Key: req.params.file_name
        };
        
        s3.getObject(getParams, function(err, data) {
            if (err){
                return res.status(400).json(utils.responseObj('Could nor get the file.', err, false));
            } else {
                return res.send(data.Body);
            }
        });
    }

    getAllGiftModel(req, res) {
        const page = req.body.page || 1;
        this.model.GiftCardModel.paginate({} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(!result.notEmpty) {
                return res.json(utils.responseObj('All gift model', new GiftCardModelTransform().withPaginate().transformCollection(result), true))
            } else {
                return res.json(
                    utils.responseObj("No gift model found", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }

    getOneGiftCard(req, res) {
        if(!mongoose.Types.ObjectId.isValid(req.body.gift_id))
            return res.json(utils.responseObj('gift_id is not valid.', {}, false))
        this.model.GiftCard.findById(req.body.gift_id , (err , card) => {
            if(err) throw err;
            if(card == null) {
                return res.json(
                    utils.responseObj("not found!", {}, false)
                );
            } else {
                this.model.GiftCardModel.populate(card, {path : 'model'}, (err, gift) => {
                    return res.json(
                        utils.responseObj('gift card information', new GiftCardTransform().transform(gift), true)
                    )
                });
            }
        })
    }

    getOneFeedback(req, res) {
        if(!mongoose.Types.ObjectId.isValid(req.params.feedback_id))
            return res.json(utils.responseObj('feedback_id is not valid.', {}, false))
        this.model.Feedback.findById(req.params.feedback_id , (err , feedback) => {
            if(err) throw err;
            if(feedback == null) {
                return res.json(
                    utils.responseObj("not found!", {}, false)
                );
            } else {
                this.model.Test.populate(feedback, {path : 'test_id'}, (err, feed) => {
                    return res.json(
                        utils.responseObj('feedback information', new FeedbackTransform().transform(feed), true)
                    )
                });
            }
        })
    }
}