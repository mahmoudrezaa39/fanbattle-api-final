const Controller = require(`${config.path.controller}/Controller`);
const TransactionTransform = require(`${config.path.transform}/v1/TransactionTransform`);
const paypal = require('paypal-rest-sdk');
const payments = paypal.v1.payments;

module.exports = new class DeveloperPaymentController extends Controller {
    getPayable(req, res) {
        req.checkBody('tester_count' , 'tester count field required').notEmpty();
        req.checkBody('incentive' , 'gift card is not valid').isMongoId();
        
        if(this.showValidationErrors(req, res)) 
            return;

        this.model.GiftCardModel.findById(req.body.incentive , (err , model) => {
            if(err) throw err;
            if(model == null) {
                return res.json(
                    utils.responseObj("This gift model does not exist.", {}, false)
                );
            } else {
                const p = (req.body.tester_count * model.amount) + (config.recruiting_fee * req.body.tester_count);
                return res.json(utils.responseObj('Payable Price', {payable_price : p}, true))
            }
        })
    }

    buyWithPaypal(req, res) {
        const env = new paypal.core.SandboxEnvironment(config.paypal.client_id, config.paypal.client_secret);
        const client = new paypal.core.PayPalHttpClient(env);
        const payment = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": `${config.domain}:${config.port}/api/v1/developer/buy-success`,
                "cancel_url": `${config.domain}:${config.port}/api/v1/developer/buy-failed`
            },
            "transactions": [{
                "amount": {
                    "total": req.params.p,
                    "currency": "USD"
                }
            }]
        }
        let request = new payments.PaymentCreateRequest();
        request.requestBody(payment);
        client.execute(request).then((response) => {
            var payId = response.result.id;
            this.createTransaction(req.user._id, req.params.p, payId);
            var link;
            response.result.links.forEach(element => {
                if(element.method == 'REDIRECT') link = element.href
            });
            return res.json(
                utils.responseObj("success", {link : link}, true)
            );
          }).catch((error) => {
            console.error(error);
                return res.json(
                    utils.responseObj("An error has occured!", {statusCode : error.statusCode , message : error.message}, false)
                );
          });
    }

    buySuccess(req, res) {
        this.updateTransaction(req.query.paymentId, req.query.PayerID);
        return res.json(
            utils.responseObj("Congratulations! Successfully completed payment.", {paymentId : req.query.paymentId}, true)
        );
    }
    buyFailed(req, res) {
        return res.json(
            utils.responseObj("Payment was canceled!", {}, false)
        );
    }

    createTransaction(userId, price, payId) {
        this.model.TransactionInfo({
            developer_id : userId,
            price : price,
            paymentId : payId
        }).save((err) => {
            if(err) throw err;
        })
    }
    updateTransaction(payId, payerID) {
        this.model.TransactionInfo.findOneAndUpdate({paymentId : payId}, {
            PayerID : payerID,
            status : true
        }, (err) => {
            if(err) throw err;
        })
    }

    getMyTransaction(req, res) {
        const page = req.body.page || 1;
        this.model.TransactionInfo.paginate({ developer_id : req.user._id } , { page , limit : config.item_count_in_page })
        .then(result => {
            if(result.docs.length > 0) {
                this.model.Test.populate(result.docs, {path : 'test_id'}, (err, data) => {
                    return res.json(
                        utils.responseObj('All Transaction', new TransactionTransform().withPaginate().transformCollection({docs : data}), true)
                    )
                });
            } else {
                return res.json(
                    utils.responseObj("No transaction found!", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }
}