const Controller = require(`${config.path.controller}/Controller`);
const Test = require(`${config.path.model}/test`);
const TestTransform = require(`${config.path.transform}/v1/TestTransform`);
const AWS = require('aws-sdk');
const s3= new AWS.S3();
const fs = require('fs');
const mongoose = require('mongoose');
const schedule = require('node-schedule');

module.exports = new class DeveloperTestController extends Controller {
    createTest(req, res) {
        req.checkBody('test_name' , 'Test name field required').notEmpty();
        req.checkBody('game_poster' , 'Game poster field required').notEmpty();
        req.checkBody('game_link' , 'Game link field required').notEmpty();
        req.checkBody('body' , 'body field required').notEmpty();
        req.checkBody('min_age' , 'age field required').notEmpty();
        req.checkBody('max_age' , 'age field required').notEmpty();
        req.checkBody('location' , 'location field required').notEmpty();
        req.checkBody('incentive' , 'gift card is not valid').isMongoId();
        req.checkBody('payment_id' , 'payment id is not valid').notEmpty();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.Test({
            developer_id : req.user._id,
            tester_count : req.body.tester_count,
            test_name : req.body.test_name,
            game_poster : req.body.game_poster,
            game_link : req.body.game_link,
            body : req.body.body,
            incentive : req.body.incentive,
            expire_data : req.body.expire_data,
            tester_info : {
                minAge : req.body.min_age,
                maxAge : req.body.max_age,
                gender : req.body.gender,
                location : req.body.location
            }
        }).save((err, test) => {
            if(err) throw err;

            this.model.TransactionInfo.findOneAndUpdate({paymentId : req.body.payment_id}, {
                test_id : test._id
            }, (err) => {
                if(err) throw err;
            })

            if(test.expire_data != null) {
                this.createSchedule(test.expire_data, test._id);
            }

            return res.json(
                utils.responseObj('test successfully created.', new TestTransform().transform(test), true)
            );
        })
    }

    editTest(req, res) {
        req.checkBody('test_name' , 'Test name field required').notEmpty();
        req.checkBody('game_poster' , 'Game poster field required').notEmpty();
        req.checkBody('game_link' , 'Game link field required').notEmpty();
        req.checkBody('body' , 'body field required').notEmpty();
        req.checkBody('min_age' , 'age field required').notEmpty();
        req.checkBody('max_age' , 'age field required').notEmpty();
        req.checkBody('location' , 'location field required').notEmpty();
        
        if(this.showValidationErrors(req, res)) 
            return;

        if(!mongoose.Types.ObjectId.isValid(req.body.test_id))
            return res.json(utils.responseObj('test_id is not valid.', {}, false));

        this.model.Test.findOneAndUpdate({_id : req.body.test_id} , {
            test_name : req.body.test_name,
            game_poster : req.body.game_poster,
            game_link : req.body.game_link,
            body : req.body.body,
            expire_data : req.body.expire_data,
            tester_info : {
                minAge : req.body.min_age,
                maxAge : req.body.max_age,
                gender : req.body.gender,
                location : req.body.location
            }
        }, (err) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('test edited successfully.', {}, true)
            );
        });
    }

    getMyTests(req, res) {
        this.model.Test.find({ developer_id : req.user._id } , (err, data) => {
            if(err) throw err;
            if(data.length > 0) {
                return res.json(utils.responseObj('All tests', data, true))
            } else {
                return res.json(
                    utils.responseObj("tests not found!", {}, true)
                );
            }
        })
    }

    uploadFile(req, res) {
        if (req.fileValidationError) {
            return res.json(
                utils.responseObj(req.fileValidationError, {}, false)
            );
        }
        fs.readFile(req.file.path, function (err, filedata) {
            if (!err) {
              const putParams = {
                  Bucket      : config.s3_bucket_keys.bucket_name,
                  Key         : req.file.filename,
                  Body        : filedata,
                  ACL         : 'public-read'
              };
              s3.putObject(putParams, function(err, data){
                if (err) {
                    return res.json(utils.responseObj('Could nor upload the file.', err, false));
                } else {
                    Test.findOneAndUpdate({_id : req.params.test_id} , {
                        $push: { game_poster : req.file.filename }
                    }, (err) => {
                        if(err) throw err;
                        res.json(utils.responseObj('Successfully uploaded the file', {}, true));
                    });
                }
              });
            } else {
                return res.json(
                    utils.responseObj(err, {}, false)
                );
            }
          });
    }

    confirmFeedback(req, res) {
        this.model.Feedback.findById(req.params.feed_id, (err, feed) => {
            if(err) throw err;
            if(feed.status == false) {
                feed.status = true,
                feed.save(async(err) => {
                    if(err) throw err;
                    this.model.GiftCard.find({status : 'private'}, (err, gift) => {
                        if(err) throw err;
                        if(gift.length == 0) {
                            return res.json(
                                utils.responseObj('gift not found!!', {}, false)
                            );
                        };
                        gift[0].status = 'public';
                        gift[0].tester_id = req.body.tester_id;
                        gift[0].save((err) => {
                            if(err) throw err
                            return res.json(
                                utils.responseObj('feedback confirmed.', {}, true)
                            );
                        });
                    });
                })
            } else {
                return res.json(
                    utils.responseObj('test confirmed.', {}, true)
                );
            }
        })
    }

    createSchedule(data, testId) {
        var parts = data.split('-');
        var date = new Date(parts[0], parts[1] - 1, parts[2], 23, 59);
        schedule.scheduleJob(date, () => {
            this.model.Test.findOneAndUpdate({_id : testId} , {
                status : 'expire'
            }, (err) => {
                if(err) throw err;
                console.log(`${testId} test expired.`);
            });
        });
    }
}