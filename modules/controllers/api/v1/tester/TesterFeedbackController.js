const Controller = require(`${config.path.controller}/Controller`);
const TestTransform = require(`${config.path.transform}/v1/TestTransform`);
const FeedbackTransform = require(`${config.path.transform}/v1/FeedbackTransform`);
const GiftCardTransform = require(`${config.path.transform}/v1/GiftCardTransform`);
const Feedback = require(`${config.path.model}/feedback`);
const AWS = require('aws-sdk');
const s3= new AWS.S3();
const fs = require('fs');

module.exports = new class TesterFeedbackController extends Controller {
    getTests(req, res) {
        this.model.TesterProfile.findOne({ tester_id : req.user._id } , (err , profile) => {
            if(err) throw err;
            if(profile == null) {
                return res.json(
                    utils.responseObj("This user does not have profile.", {}, false)
                );
            } else {
                var today = new Date();
                var birthDate = new Date(profile.birthday.year,profile.birthday.month,profile.birthday.day);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = ( today.getMonth() + 1 ) - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                var location = profile.location;
                var gender = profile.gender;
                const page = req.body.page || 1;
                this.model.Test.paginate({
                    'tester_info.minAge' : {'$lte' : age},
                    'tester_info.maxAge' : {'$gte' : age},
                    'tester_info.gender' : gender,
                    'tester_info.location' : location,
                    status : 'public'
                } , { page , limit : config.item_count_in_page })
                .then(result => {
                    if(result.docs.length > 0) {
                        return res.json(utils.responseObj('All tests', new TestTransform().withPaginate().transformCollection(result), true))
                    } else {
                        return res.json(
                            utils.responseObj("No tests found!", {}, true)
                        );
                    }
                })
                .catch(err => console.log(err));
            }
        })
    }

    createFeedback(req, res) {
        req.checkParams('test_id' , 'test not valid!').isMongoId();
        req.checkBody('video_link' , 'video link field required.').notEmpty();
        req.checkBody('body' , 'body field required.').notEmpty();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.Test.findOne({ _id : req.params.test_id } , (err , test) => {
            if(err) throw err;
            if(test == null) {
                return res.json(
                    utils.responseObj("test not found!", {}, false)
                );
            } else {
                let flag = true;
                test.testers_id.forEach(element => {
                    if(element.equals(req.user._id)) {
                        flag = false;
                        return res.json(
                            utils.responseObj("You have already commented.", {}, false)
                        );
                    }
                });
                if(flag) {
                    this.model.Feedback({
                        test_id : req.params.test_id,
                        tester_id : req.user._id,
                        video_link : req.body.video_link,
                        body : req.body.body
                    }).save((err, feedback) => {
                        if(err) throw err;
                        this.updateTest(req.params.test_id, req.user._id, feedback._id);
                        return res.json(
                            utils.responseObj('feedback successfully created.', new FeedbackTransform().transform(feedback), true)
                        );
                    })
                }
            }
        })
    }

    updateTest(testId, testerId, feedbackId) {
        this.model.Test.findOne({_id : testId}, (err, test) => {
            if(err) throw err;
            if((test.tester_count) == (test.testers_id.length + 1)) test.status = 'expire';
            test.feedback.push(feedbackId);
            test.testers_id.push(testerId);
            test.save((err) => {if(err) throw err})
        });
    }

    getMyFeedback(req, res) {
        const page = req.body.page || 1;
        this.model.Feedback.paginate({tester_id : req.user._id} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(result.docs.length > 0) {
                this.model.Test.populate(result.docs, {path : 'test_id'}, (err, feeds) => {
                    return res.json(utils.responseObj('All feedback', new FeedbackTransform().withPaginate().transformCollection({docs : feeds}), true))
                });
            } else {
                return res.json(
                    utils.responseObj("No feedback found", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }

    getMyGifts(req, res) {
        const page = req.body.page || 1;
        this.model.GiftCard.paginate({tester_id : req.user._id} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(result.docs.length > 0) {
                this.model.GiftCardModel.populate(result.docs, {path : 'model'}, (err, gifts) => {
                    return res.json(utils.responseObj('All gifts', new GiftCardTransform().withPaginate().transformCollection({docs : gifts}), true))
                });
            } else {
                return res.json(
                    utils.responseObj("No gifts found", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }

    uploadFile(req, res) {
        if (req.fileValidationError) {
            return res.json(
                utils.responseObj(req.fileValidationError, {}, false)
            );
        }
        fs.readFile(req.file.path, function (err, filedata) {
            if (!err) {
              const putParams = {
                  Bucket      : config.s3_bucket_keys.bucket_name,
                  Key         : req.file.filename,
                  Body        : filedata,
                  ACL         : 'public-read'
              };
              s3.putObject(putParams, function(err, data){
                if (err) {
                    return res.json(utils.responseObj('Could nor upload the file.', err, false));
                } else {
                    Feedback.findOneAndUpdate({_id : req.params.feedback_id} , {
                        video_link : req.file.filename
                    }, (err) => {
                        if(err) throw err;
                        res.json(utils.responseObj('Successfully uploaded the file', {}, true));
                    });
                }
              });
            } else {
                return res.json(
                    utils.responseObj(err, {}, false)
                );
            }
          });
    }
}


