const Controller = require(`${config.path.controller}/Controller`);
const TesterProfileTransform = require(`${config.path.transform}/v1/TesterProfileTransform`);

module.exports = new class TesterProfileController extends Controller {
    createProfile(req, res) {
        req.checkBody('day' , 'day field required').notEmpty();
        req.checkBody('month' , 'month field required').notEmpty();
        req.checkBody('year' , 'year field required').notEmpty();
        req.checkBody('location' , 'location field required').notEmpty();
        
        if(this.showValidationErrors(req, res)) 
            return;

        this.model.TesterProfile({
            tester_id : req.user._id,
            birthday : {
                day : req.body.day,
                month : req.body.month,
                year : req.body.year
            },
            gender : req.body.gender,
            location : req.body.location
        }).save((err, profile) => {
            if(err) {
                if(err.code == 11000) {
                    return res.json(
                        utils.responseObj('this profile already exists.', {}, false)
                    );
                } else {
                    throw err;
                }
            }
            return res.json(
                utils.responseObj('profile create successfully.', new TesterProfileTransform().transform(profile), true)
            );
        });
    }

    getProfile(req , res) {
        this.model.TesterProfile.findOne({ tester_id : req.user._id } , (err , profile) => {
            if(err) throw err;
            if(profile == null) {
                return res.json(
                    utils.responseObj("This user does not have profile.", {}, false)
                );
            } else {
                return res.json(utils.responseObj('profile information', new TesterProfileTransform().transform(profile), true))
            }
        })
    }

    editProfile(req, res) {
        req.checkBody('day' , 'day field required').notEmpty();
        req.checkBody('month' , 'month field required').notEmpty();
        req.checkBody('year' , 'year field required').notEmpty();
        req.checkBody('gender' , 'gender field required').notEmpty();
        req.checkBody('location' , 'location field required').notEmpty();
        
        if(this.showValidationErrors(req, res)) 
            return;
        
        this.model.TesterProfile.findOneAndUpdate({tester_id : req.user._id} , {
            birthday : {
                day : req.body.day,
                month : req.body.month,
                year : req.body.year
            },
            gender : req.body.gender,
            location : req.body.location
        }, (err) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('profile edited successfully.', {}, true)
            );
        });
    }
}