const Controller = require(`${config.path.controller}/Controller`);
const UserTransform = require(`${config.path.transform}/v1/UserTransform`);
const bcrypt = require('bcrypt');
const SocialUserTransform = require(`${config.path.transform}/v1/SocialUserTransform`);

module.exports = new class AuthController extends Controller {
    register(req , res) {  
        req.checkBody('email' , 'email field required').notEmpty();
        req.checkBody('email' , 'email is incorrect').isEmail();
        req.checkBody('password' , 'password field required').notEmpty();
        req.checkBody('type' , 'type field required').notEmpty();
        
        if(this.showValidationErrors(req, res)) 
            return;

        this.model.User({
            type : req.body.type,
            email : req.body.email,
            password : req.body.password
        }).save((err, user) => {
            if(err) {
                if(err.code == 11000) {
                    return res.json(
                        utils.responseObj('this email already exists.', {}, false)
                    );
                } else {
                    throw err;
                }
            }

            return res.json(
                utils.responseObj('signup successfully done.', new UserTransform().transform(user,true), true)
            );
        })
    }

    login(req , res) {
        req.checkBody('email' , 'email field required').notEmpty();
        req.checkBody('password' , 'password field required').notEmpty();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.User.findOne({ email : req.body.email } , (err , user) => {
            if(err) throw err;

            if(user == null) {
                return res.json(
                    utils.responseObj("The email and password are incorrect.", {}, false)
                );
            }

            bcrypt.compare(req.body.password , user.password , (err , status) => {

                if(!status) {
                    return res.json(
                        utils.responseObj("The password is incorrect.", {}, false)
                    );
                }
                return res.json(
                    utils.responseObj("signin successfully done.", new UserTransform().transform(user,true), true)
                );     
            })
        })

    }

    socialLogin(req, res) {
        this.model.User.findOne({ email : req.body.email } , (err , user) => {
            if(err) throw err;
            if(user == null) {
                this.model.User({
                    email : req.body.email
                }).save((err, user) => {
                    if(err) throw err;
                    return res.json(
                        utils.responseObj('signup successfully done.', new SocialUserTransform().transform(user,true), true)
                    );
                })
            } else {
                return res.json(
                    utils.responseObj("signin successfully done.", new SocialUserTransform().transform(user,true), true)
                );
            }
        })
    }
}