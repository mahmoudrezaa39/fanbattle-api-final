const Controller = require(`${config.path.controller}/Controller`);
const GiftCardTransform = require(`${config.path.transform}/v1/GiftCardTransform`);
const GiftCardModelTransform = require(`${config.path.transform}/v1/GiftCardModelTransform`);

module.exports = new class AdminGiftCardController extends Controller {
    createGift(req, res) {
        req.checkBody('code' , 'code field required').notEmpty();
        req.checkBody('model' , 'model id no valid').isMongoId();
        
        if(this.showValidationErrors(req, res)) 
            return;

        this.model.GiftCard({
            creator : req.user._id,
            code : req.body.code,
            model : req.body.model
        }).save((err, gift) => {
            if(err) {
                if(err.code == 11000) {
                    return res.json(
                        utils.responseObj('this gift card used.', {}, false)
                    );
                } else {
                    throw err;
                }
            }
            this.model.GiftCardModel.populate(gift, {path : 'model'}, (err, gift) => {
                return res.json(
                    utils.responseObj('gift card successfully created.', new GiftCardTransform().transform(gift), true)
                );
            });
        })
    }

    createListOfGift(req, res) {
        req.checkBody('model' , 'model id no valid').isMongoId();
        req.checkBody('docs' , 'no doc found!').isLength({ min: 1 });
        
        if(this.showValidationErrors(req, res)) 
            return;

        let arr = req.body.docs;
        if(arr.isLength >= 100) return utils.responseObj('doc is too large! (Max size: 100)', {}, false);
        arr.forEach(element => {
            element.creator = req.user._id;
            element.model = req.body.model;
        });
        this.model.GiftCard.insertMany(arr, (err, docs) => {
            if(err) {
                if(err.code == 11000) {
                    return res.json(
                        utils.responseObj(`some gift cards used. docs entered until item '${err.insertedDocs[0].code}', please check the rest`, {}, false)
                    );
                } else {
                    throw err;
                }
            }
            return res.json(
                utils.responseObj('gift cards successfully created.', {}, true)
            )
        });
    }

    editGift(req, res) {
        req.checkBody('code' , 'code field required').notEmpty();
        req.checkBody('model_id' , 'model_id is not valid.').isMongoId();
        req.checkParams('gift_id' , 'gift_id is not valid.').isMongoId();

        if(this.showValidationErrors(req, res)) 
            return;
        
        this.model.GiftCard.findOneAndUpdate({_id : req.params.gift_id, status : 'private'} , {
            model_id : req.body.model_id,
            code : req.body.code
        }, (err) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('gift card edited successfully.', {}, true)
            );
        });
    }

    deleteGift(req, res) {
        req.checkParams('gift_id' , 'gift_id is not valid.').isMongoId();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.GiftCard.findByIdAndRemove(req.params.gift_id , (err) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('gift card deleted successfully.', {}, true)
            )
        })
    }

    getListOfGift(req, res) {
        const page = req.body.page || 1;
        this.model.GiftCard.paginate({status : 'private'} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(!result.notEmpty) {
                this.model.GiftCardModel.populate(result.docs, {path : 'model'}, (err, gifts) => {
                    return res.json(
                        utils.responseObj('All gift card', new GiftCardTransform().withPaginate().transformCollection({docs : gifts}), true)
                    )
                });
            } else {
                return res.json(
                    utils.responseObj("No gift card found", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }



    createGiftModel(req, res) {
        req.checkBody('amount' , 'amount field required').notEmpty();
        
        if(this.showValidationErrors(req, res)) 
            return;

        this.model.GiftCardModel({
            creator : req.user._id,
            amount : req.body.amount,
            type : req.body.type
        }).save((err, model) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('gift card model successfully created.', new GiftCardModelTransform().transform(model), true)
            );
        })
    }

    editGiftModel(req, res) {
        req.checkBody('amount' , 'amount field required').notEmpty();
        req.checkBody('type' , 'type field required').notEmpty();
        req.checkParams('gift_model_id' , 'gift_model_id is not valid.').isMongoId();

        if(this.showValidationErrors(req, res)) 
            return;
        
        this.model.GiftCardModel.findById(req.params.gift_model_id , (err, model) => {
            if(err) throw err;
            if(model == null) {
                return res.json(
                    utils.responseObj('gift card model not found!', {}, false)
                );
            }
            model.set({
                type : req.body.type,
                amount : req.body.amount
            }).save((err, model) => {
                if(err) throw err;
                return res.json(
                    utils.responseObj('gift card model edited successfully.', new GiftCardModelTransform().transform(model), true)
                );
            });
        });
    }

    deleteGiftModel(req, res) {
        req.checkParams('gift_model_id' , 'gift_model_id is not valid.').isMongoId();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.GiftCardModel.findByIdAndRemove(req.params.gift_model_id , (err) => {
            if(err) throw err;
            return res.json(
                utils.responseObj('gift card model deleted successfully.', {}, true)
            )
        })
    }
}