const Controller = require(`${config.path.controller}/Controller`);
const FeedbackTransform = require(`${config.path.transform}/v1/FeedbackTransform`);

module.exports = new class AdminTestController extends Controller {

    getAllTest(req, res) {
        this.model.Test.find({} , (err, data) => {
            if(err) throw err;
            if(data.length > 0) {
                return res.json(utils.responseObj('All tests', data, true))
            } else {
                return res.json(
                    utils.responseObj("tests not found!", {}, true)
                );
            }
        })
    }
    
    publicTest(req, res) {
        req.checkBody('status' , 'Test status field required').notEmpty();
        req.checkParams('test_id' , 'test_id is not valid.').isMongoId();

        if(this.showValidationErrors(req, res)) 
            return;

        this.model.Test.findOneAndUpdate({_id : req.params.test_id} , {
            status : req.body.status
        }, (err) => {
            if(err) throw err;
            console.log(new Date)
            return res.json(
                utils.responseObj('test published.', {}, true)
            );
        });
    }

    getAllFeedback(req, res) {
        const page = req.body.page || 1;
        this.model.Feedback.paginate({} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(!result.notEmpty) {
                this.model.Test.populate(result.docs, {path : 'test_id'}, (err, feeds) => {
                    return res.json(utils.responseObj('All feedback', new FeedbackTransform().withPaginate().transformCollection({docs : feeds}), true))
                });
            } else {
                return res.json(
                    utils.responseObj("No feedback found", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }
}