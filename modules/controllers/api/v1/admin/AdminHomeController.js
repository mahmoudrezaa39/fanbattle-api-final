const Controller = require(`${config.path.controller}/Controller`);
const TransactionTransform = require(`${config.path.transform}/v1/TransactionTransform`);

module.exports = new class AdminHomeController extends Controller {
    index(req, res) {
        res.json('Admin home page!');    
    }

    getAllTransaction(req, res) {
        const page = req.body.page || 1;
        this.model.TransactionInfo.paginate({} , { page , limit : config.item_count_in_page })
        .then(result => {
            if(result.docs.length > 0) {
                this.model.Test.populate(result.docs, {path : 'test_id'}, (err, data) => {
                    return res.json(
                        utils.responseObj('All Transaction', new TransactionTransform().withPaginate().transformCollection({docs : data}), true)
                    )
                });
            } else {
                return res.json(
                    utils.responseObj("No transaction found!", {}, false)
                );
            }
        })
        .catch(err => console.log(err));
    }
}