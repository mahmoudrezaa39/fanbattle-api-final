// Model
const User = require(`${config.path.model}/user`);
const TesterProfile = require(`${config.path.model}/testerProfile`);
const Test = require(`${config.path.model}/test`);
const Feedback = require(`${config.path.model}/feedback`);
const GiftCard = require(`${config.path.model}/giftCard`);
const GiftCardModel = require(`${config.path.model}/giftCardModel`);
const TransactionInfo = require(`${config.path.model}/transactionInfo`);

module.exports = class Controller {
    constructor() {
        this.model = { User , TesterProfile , Test , GiftCard , GiftCardModel , TransactionInfo , Feedback }
    }

    showValidationErrors(req , res , callback) {
        let errors = req.validationErrors();
        if(errors) {
            res.status(422).json({
                message : errors.map(error => {
                    return {
                        'field' : error.param,
                        'message' : error.msg
                    }
                }),
                success : false
            });
            return true;
        }
        return false
    }


    escapeAndTrim(req , items) {
        items.split(' ').forEach(item => {
            req.sanitize(item).escape();
            req.sanitize(item).trim();            
        });
    }
}