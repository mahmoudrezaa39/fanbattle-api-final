const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose'); 
const expressValidator = require('express-validator');
const AWS = require('aws-sdk');
const Utils = require('./modules/Utils');
global.config = require('./modules/config');
global.utils = new Utils();

//Connect to DB
mongoose.connect(config.mongo_uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex : true,
  useFindAndModify: false
}).then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error(`Could not connect to MongoDB...\n${err}`));

// S3 buckt config
AWS.config.update({
  accessKeyId: config.s3_bucket_keys.iam_access_id,
  secretAccessKey: config.s3_bucket_keys.iam_secret,
  region: config.s3_bucket_keys.region,
});

app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json({ type : 'application/json' }));
app.use(expressValidator());
app.use('/public' , express.static('public'));

const apiRouter = require('./modules/routes/api');
app.use('/api' , apiRouter);

app.listen(config.port, () => console.log(`listening at port ${config.port}`))
